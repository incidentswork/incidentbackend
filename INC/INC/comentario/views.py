from django.shortcuts import render

from rest_framework.decorators import api_view, permission_classes
from django.http.response import JsonResponse
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import comentario
from .serializers import ComentarioSerializer, AllComentarioSerializer


@api_view(['GET', 'POST', 'DELETE'])
def comentarios_list(request):
    if request.method == 'GET':
        comentarios = comentario.objects.all()

        titulo = request.GET.get('comentario', None)
        if titulo is not None:
            tutorials = comentarios.filter(titulo__icontains=titulo)

        comentarios_serializer = ComentarioSerializer(comentarios, many=True)
        return JsonResponse(comentarios_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        comentarios_data = JSONParser().parse(request)
        comentarios_serializer = ComentarioSerializer(data=comentarios_data)
        if comentarios_serializer.is_valid():
            comentarios_serializer.save()
            return JsonResponse(comentarios_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(comentarios_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # elif request.method == 'DELETE':
    #    count = post.objects.all().delete()
    #    return JsonResponse({'message': '{} El Post Borrado Exitosamente!'.format(count[0])},
    #                        status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
def comentario_detalle(request, pk):
    try:
        comentarios = comentario.objects.get(pk=pk)
    except comentario.DoesNotExist:
        return JsonResponse({'message': 'Este Comentario no Existe.'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        comentario_serializer = ComentarioSerializer(comentarios)
        return JsonResponse(comentario_serializer.data)

    elif request.method == 'PUT':
        comentario_data = JSONParser().parse(request)
        comentario_serializer = ComentarioSerializer(comentarios, data=comentario_data)
        if comentario_serializer.is_valid():
            comentario_serializer.save()
            return JsonResponse(comentario_serializer.data)
        return JsonResponse(comentario_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        comentarios.delete()
        return JsonResponse({'message': 'Comentario Borrado Exitosamente!'}, status=status.HTTP_204_NO_CONTENT)


#ComentarioPost
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def comentario_post(request, pk):
    if request.method == 'GET':
        paginator = PageNumberPagination()
        paginator.page_size = 5

        comentarios = comentario.objects.filter(id_post=pk, autorizado=True).order_by('-id')

        if not comentarios:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            result_page = paginator.paginate_queryset(comentarios, request)
            comentarios_serializer = AllComentarioSerializer(result_page, many=True)

        return paginator.get_paginated_response(comentarios_serializer.data)


#DeniComentarioPost
@api_view(['GET'])
def denicomentario_post(request, pk):
    if request.method == 'GET':
        paginator = PageNumberPagination()
        paginator.page_size = 5

        comentarios = comentario.objects.filter(id_post=pk, autorizado=False).order_by('-id')

        if not comentarios:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            result_page = paginator.paginate_queryset(comentarios, request)
            comentarios_serializer = AllComentarioSerializer(result_page, many=True)

        return paginator.get_paginated_response(comentarios_serializer.data)



class ComentarioAPIListView(ListAPIView):
    queryset = comentario.objects.filter(id_post=10)
    serializer_class = AllComentarioSerializer
    pagination_class = PageNumberPagination

