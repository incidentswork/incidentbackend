from rest_framework import serializers
from .models import comentario
from ..core.serializers import AllUsersSerializers


class ComentarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = comentario
        fields = '__all__'


class AllComentarioSerializer(serializers.ModelSerializer):
    id_usuario = AllUsersSerializers(many=False, read_only=True)
    class Meta:
        model = comentario
        fields = '__all__'