from django.db import models


# Create your models here.
from INC.post.models import post
from django.contrib.auth.models import User



class comentario(models.Model):
    comentario = models.CharField(max_length=240, blank=False, default='')
    autorizado = models.BooleanField(default=False)
    id_post = models.ForeignKey(post, on_delete=models.CASCADE, null=False, blank=False)
    id_usuario = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'comentario'
        verbose_name_plural = 'comentarios'
        ordering = ['id']

    def __str__(self):
        return self.nombre
