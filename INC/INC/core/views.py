from django.http import JsonResponse
from django.shortcuts import render

# 'Importaciones' Librerias
from rest_framework import viewsets, generics
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken import views

from rest_framework import status
from django.contrib.auth import login, logout, authenticate

# Otros
from django.contrib.auth.models import User

from .models import rol, user_role
from .serializers import AllUsersSerializers, AllRolesSerializers, ListAsignacionRoles, AsigUsRol


# LogOut
class Logout(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        request.user.auth_token.delete()
        logout(request)
        Content = 'Session Cerrada'
        return Response(Content, status=status.HTTP_200_OK)

class InfoPublica(APIView):
    def get(self, request):
        content = 'Esta es Informacion publica, que es consumida desde la API'
        return Response(content)


class InfoDeveloper(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        content = 'Bienvenido Developer'
        return Response(content)


class InfoAdmin(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        content = 'Bienvenido Admin'
        return Response(content)


class InfoModerador(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        content = 'Bienvenido Moderador'
        return Response(content)


class InfoUsuario(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        content = 'Bienvenido Usuario'
        return Response(content)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def UpdateLatestr(*args, **kwargs):
    last_user = User.objects.last()
    ultimo = AllUsersSerializers(last_user)
    usuario_instance = rol.objects.get(nombre='Usuario')
    obj = user_role.objects.create(id_usuario_id=ultimo.data['id'], id_rol=usuario_instance)
    obj.save()
    return Response(ultimo.data['id'], status=200)


# ViewSetUsuarios
class UsersViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = User.objects.all()
    serializer_class = AllUsersSerializers


# Roles
class Rol(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = rol.objects.all()
    serializer_class = AllRolesSerializers


# AsignacionRol
class AsignacionRol(generics.ListCreateAPIView):
    permission_classes = [IsAuthenticated]
    queryset = user_role.objects.all()
    serializer_class = ListAsignacionRoles


@api_view(['GET'])
def retrieve(request, pk):
    user = user_role.objects.get(id_usuario=pk)
    if request.method == 'GET':
        user_select = ListAsignacionRoles(user)
        return JsonResponse(user_select.data)


# TokenPersonalizado
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token


class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        as_role = user_role.objects.filter(id_usuario__gte=user.pk).values_list()
        # as_role = user_role.objects.filter(id=user.pk).values_list()
        role = rol.objects.filter(id=as_role[0][2]).values_list()
        # role = user_role.objects.get(id_usuario=user.pk)
        # role_select = ListAsignacionRoles(role)

        return Response({
            'id': user.pk,
            'username': user.username,
            # 'first_name': user.first_name,
            # 'last_name': user.last_name,
            'email': user.email,
            'roles': [role[0][1]],
            'accessToken': token.key,
        })
