from rest_framework import serializers
from django.contrib.auth.models import User

from .models import rol, user_role


class AllUsersSerializers(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')
        #fields = '__all__'

class AllRolesSerializers(serializers.ModelSerializer):
    class Meta:
        model = rol
        fields = ('id', 'nombre')

class ListAsignacionRoles(serializers.ModelSerializer):
    #id_usuario = AllUsersSerializers(many=False, read_only=True)
    id_rol = AllRolesSerializers(many=False, read_only=True)
    class Meta:
        model = user_role
        fields = ('id_usuario_id', 'id_rol')

class AsigUsRol(serializers.ModelSerializer):
    class Meta:
        model = user_role
        fields = '__all__'

