from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class rol(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length=40)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'rol'
        verbose_name_plural = 'roles'
        ordering = ['id']

    def __str__(self):
        return self.nombre


class user_role(models.Model):
    id = models.AutoField(primary_key=True),
    id_usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    # id_rol = models.OneToOneField(rol, on_delete=models.CASCADE) models.ManyToManyField(Publication)
    id_rol = models.ForeignKey(rol, null=False, blank=False, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'user_role',
        verbose_name_plural = 'user_roles',
        ordering = ['id']

    def __str__(self):
        return self.id, self.id_usuario, self.id_rol
