"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import rest_framework
from django.conf.urls import url
from django.contrib import admin
from django.urls import path

from .core import views
from .core.views import Rol, AsignacionRol, CustomAuthToken, retrieve
from .post.views import post_list, post_detalle, autorizado, no_autorizado, autorizar, rechazado, PostAPIListView, \
    MePost, DeniPostAPIListView, CountMePost, CountMePostDeni

from .comentario.views import comentarios_list, comentario_detalle, ComentarioAPIListView, comentario_post, \
    denicomentario_post
from .registrador import RegisterUserAPI


users_data_list = views.UsersViewSet.as_view({
    'get':'list'
    #'post':'create'
})
users_data_detail = views.UsersViewSet.as_view({
    'get':'retrieve',
    'put':'update',
    'patch':'partial_update',
    'delete':'destroy'
})


urlpatterns = [
    path('admin/', admin.site.urls),
    url('api/registrar/', RegisterUserAPI.as_view(), name='registrar_usuario'),
    # Login and Logout
    # path('api/login/', rest_framework.authtoken.views.obtain_auth_token),
    url('api/login/', CustomAuthToken.as_view(), name='api-token-auth'),
    path('api/logout/', views.Logout.as_view()),

    url('api/usuarios', users_data_list, name='usuarios'),
    url(r'^api/usuarios/(?P<pk>[0-9]+)/$', users_data_detail, name='usuarios_detalle'),
    path('api/rol', Rol.as_view(), name='rol'),
    path('api/asignacion_rol', AsignacionRol.as_view(), name='asignacion_rol'),
    url('api/select/(?P<pk>[0-9]+)/$', retrieve),

    # Publico&Usuarios
    url('api/test/public', views.InfoPublica.as_view()),
    url('api/test/usuario', views.InfoUsuario.as_view()),
    url('api/test/mod', views.InfoModerador.as_view()),
    url('api/test/admin', views.InfoAdmin.as_view()),

    # Prueba
    url('api/ultimo', views.UpdateLatestr, name='ultimo'),

    #Post
    url(r'^api/post$', post_list, name='post'),
    url(r'^api/post/(?P<pk>[0-9]+)$', post_detalle),
    url(r'^api/autorizados', autorizado, name='pautorizados'),
    url(r'^api/no_autorizado', no_autorizado, name='no_autorizado'),
    url(r'^api/rechazados/(?P<pk>[0-9]+)$', rechazado, name='rechazados'),
    url(r'^api/autorizar/(?P<pk>[0-9]+)$', autorizar, name='autorizar'),

    #Post Autorizados
    url(r'^api/pag', PostAPIListView.as_view(), name='pag'),
    #Post Denegados
    url(r'^api/denpag', DeniPostAPIListView.as_view(), name='denpag'),
    url(r'^api/me_post/(?P<pk>[0-9]+)$', MePost, name='me_post'),
    url(r'^api/com_pag', ComentarioAPIListView.as_view(), name='com_pag'),
    url(r'^api/comentpost/(?P<pk>[0-9]+)$', comentario_post, name='comentpost'),
    url(r'^api/dcomentpost/(?P<pk>[0-9]+)$', denicomentario_post, name='dcomentpost'),
    #url(r'^api/autcomentpost/(?P<pk>[0-9]+)$', AuthComentarioPost, name='autcomentpost'),4

    #Count
    url(r'^api/countpost/(?P<pk>[0-9]+)$', CountMePost, name='countpost'),
    url(r'^api/dcountpost/(?P<pk>[0-9]+)$', CountMePostDeni, name='dcountpost'),

    #Comentario
    url(r'^api/comentario$', comentarios_list, name='comentario'),
    url(r'^api/comentario/(?P<pk>[0-9]+)$', comentario_detalle),

]
