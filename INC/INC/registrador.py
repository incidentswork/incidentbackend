from rest_framework.authtoken.admin import User
from rest_framework.response import Response

from .core.models import user_role, rol
from .core.serializers import AllUsersSerializers
from .serializers import UserSerializers
from rest_framework.views import APIView
from rest_framework import status


class RegisterUserAPI(APIView):
    def post(self, request):
        serializer = UserSerializers(data=request.data)
        if serializer.is_valid():
            user = serializer.save()
            last_user = User.objects.last()
            ultimo = AllUsersSerializers(last_user)
            usuario_instance = rol.objects.get(nombre='Usuario')
            obj = user_role.objects.create(id_usuario_id=ultimo.data['id'], id_rol=usuario_instance)
            obj.save()
            mensaje = 'Usuario Registrado con Exito'
            #return Response(serializer.data, status=status.HTTP_201_CREATED)
            return  Response({
            'message': mensaje,
            })
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
