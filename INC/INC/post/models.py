from django.db import models

from django.contrib.auth.models import User

# Create your models here. BooleanField
class post(models.Model):
    titulo = models.CharField(max_length=70, blank=False, default='')
    description = models.CharField(max_length=500, blank=False, default='')
    autorizado = models.BooleanField(default=False)
    rechazado = models.BooleanField(default=False)
    id_usuario = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo