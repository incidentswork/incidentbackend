from rest_framework import serializers
from .models import post
from ..core.serializers import AllUsersSerializers


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = post
        fields = '__all__'

class AllPostSerializer(serializers.ModelSerializer):
    id_usuario = AllUsersSerializers(many=False, read_only=True)
    class Meta:
        model = post
        fields = '__all__'


