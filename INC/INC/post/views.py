from django.shortcuts import render

from rest_framework.decorators import api_view, permission_classes
from django.http.response import JsonResponse
from rest_framework.generics import ListAPIView
from rest_framework.parsers import JSONParser
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import post
from .serializers import PostSerializer, AllPostSerializer
from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination


@api_view(['GET', 'POST', 'DELETE'])
@permission_classes([IsAuthenticated])
def post_list(request):
    if request.method == 'GET':
        posts = post.objects.all()

        titulo = request.GET.get('titulo', None)
        if titulo is not None:
            tutorials = posts.filter(titulo__icontains=titulo)

        posts_serializer = PostSerializer(posts, many=True)
        return JsonResponse(posts_serializer.data, safe=False)
        # 'safe=False' for objects serialization

    elif request.method == 'POST':
        post_data = JSONParser().parse(request)
        post_serializer = PostSerializer(data=post_data)
        if post_serializer.is_valid():
            post_serializer.save()
            return JsonResponse(post_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(post_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # elif request.method == 'DELETE':
    #    count = post.objects.all().delete()
    #    return JsonResponse({'message': '{} El Post Borrado Exitosamente!'.format(count[0])},
    #                        status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
def post_detalle(request, pk):
    try:
        posts = post.objects.get(pk=pk)
    except post.DoesNotExist:
        return JsonResponse({'message': 'Este Post no Existe.'}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        post_serializer = PostSerializer(posts)
        return JsonResponse(post_serializer.data)

    elif request.method == 'PUT':
        post_data = JSONParser().parse(request)
        post_serializer = PostSerializer(posts, data=post_data)
        if post_serializer.is_valid():
            post_serializer.save()
            return JsonResponse(post_serializer.data)
        return JsonResponse(post_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        posts.delete()
        return JsonResponse({'message': 'Post Borrado Exitosamente!'}, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def autorizado(request):
    posts = post.objects.filter(autorizado=True)
    if request.method == 'GET':
        post_serializer = PostSerializer(posts, many=True)
        return JsonResponse(post_serializer.data, safe=False)

@api_view(['GET'])
@permission_classes([IsAuthenticated])
def no_autorizado(request):
    posts = post.objects.filter(autorizado=False)
    if request.method == 'GET':
        post_serializer = PostSerializer(posts, many=True)
        return JsonResponse(post_serializer.data, safe=False)

@api_view(['PUT'])
@permission_classes([IsAuthenticated])
def autorizar(request, pk):
    posts = post.objects.get(pk=pk)
    if request.method == 'PUT':
        post_data = JSONParser().parse(request)
        post_serializer = PostSerializer(posts, data=post_data)
        if post_serializer.is_valid():
            post_serializer.save()
            return JsonResponse(post_serializer.data)
        return JsonResponse(post_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def rechazado(request, pk):
    posts = post.objects.filter(autorizado=False, rechazado=True, id_usuario=pk)
    if request.method == 'GET':
        post_serializer = PostSerializer(posts, many=True)
        return JsonResponse(post_serializer.data, safe=False)


class PostListAPI(ListAPIView):
    permission_classes = [IsAuthenticated]
    pagination_class = LimitOffsetPagination#PageNumberPagination
    def get_queryset(self, request, pk, *args, **kwargs,):
        posts = post.objects.filter(autorizado=False, rechazado=True, id_usuario=pk)
        post_serializer = PostSerializer(posts, many=True)
        return post_serializer

#ComentarioPost
@api_view(['GET'])
@permission_classes([IsAuthenticated])
def MePost(request, pk):
    if request.method == 'GET':
        paginator = PageNumberPagination()
        paginator.page_size = 5

        mposts = post.objects.filter(id_usuario=pk, autorizado=True).order_by('-id')

        if not mposts:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            result_page = paginator.paginate_queryset(mposts, request)
            mposts_serializer = PostSerializer(result_page, many=True)

        return paginator.get_paginated_response(mposts_serializer.data)

#AutorizacionComentarioPost

class PostAPIListView(ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = post.objects.filter(autorizado=True, rechazado=False)
    serializer_class = AllPostSerializer
    pagination_class = PageNumberPagination


class DeniPostAPIListView(ListAPIView):
    permission_classes = [IsAuthenticated]
    queryset = post.objects.filter(autorizado=False, rechazado=False)
    serializer_class = AllPostSerializer
    pagination_class = PageNumberPagination

#ComentarioPost
@api_view(['GET'])
def CountMePost(request, pk):
    if request.method == 'GET':
        paginator = PageNumberPagination()
        paginator.page_size = 5
        mposts = post.objects.filter(id_usuario=pk, autorizado=True).order_by('-id')
        if not mposts:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            result_page = paginator.paginate_queryset(mposts, request)
            mposts_serializer = PostSerializer(result_page, many=True)

        return paginator.get_paginated_response(mposts_serializer.data)


@api_view(['GET'])
def CountMePostDeni(request, pk):
    if request.method == 'GET':
        paginator = PageNumberPagination()
        paginator.page_size = 5
        mposts = post.objects.filter(id_usuario=pk, autorizado=False, rechazado=True).order_by('-id')
        if not mposts:
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            result_page = paginator.paginate_queryset(mposts, request)
            mposts_serializer = PostSerializer(result_page, many=True)

        return paginator.get_paginated_response(mposts_serializer.data)

